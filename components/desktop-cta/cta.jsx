import React from 'react';
import { useSelector } from 'react-redux';

import { Notification } from './notifications';
//import { PopUp } from './popups';

export function CTARenderer() {
    const cta = useSelector(state=>state.cta);

    return (
        <div style={{
            position: 'absolute',
            right: 0,
            top: 0
        }}>
        {Object.values(cta.notifications).map(notif=>{
            return <Notification data={notif}/>
        })}
        </div>
    );
}
