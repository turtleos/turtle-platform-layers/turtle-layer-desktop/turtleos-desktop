import React from 'react';

import installlogo from './install.png';
import { useSelector, useDispatch } from 'react-redux';

function ClockWidget(props) {
    const wallpaper = useSelector(state=>state.wallpaper);
    const lang = useSelector(state=>state.lang);
    const time = useSelector(state=>state.internal.clock);

    return (
        <div style={{
            placeSelf:'end',
            padding:'0.4rem'
        }}>
          <div style={{
              fontFamily:'Shadows Into Light',
              color:wallpaper.theme.textOnWallpaper,
              fontSize:'2.5rem',
              fontWeight:'900',
              display:'flex',
              flexDirection:'column',
              alignItems:'center',
              justifyContent:'center'
          }}>
            <div style={{
                fontSize:'2.9rem'
            }}>
              {(time.getHours()<10)?'0':''}{time.getHours()}:{(time.getMinutes()<10)?'0':''}{time.getMinutes()}:{(time.getSeconds()<10)?'0':''}{time.getSeconds()}
            </div>
            <div style={{
                height:'2px',
                width:'80%',
                opacity:0.6,
                background:wallpaper.theme.textOnWallpaper,
                borderRadius:'15px'
            }}></div>
            <div style={{
                fontSize:'2.3rem'
            }}>
             {time.getDate()}.{time.getMonth()+1}.{time.getFullYear()} {lang.translate(`datetime.days.${time.getDay()}`)}
            </div>
        </div>
        </div>
    );
}

function InstallHardDiskWidget(props) {
    const wallpaper = useSelector(state=>state.wallpaper);
    const lang = useSelector(state=>state.lang);
    const dispatch = useDispatch();
    return (
        <div style={{
            color: wallpaper.theme.textOnWallpaper,
            width:'5rem',
            alignSelf:'self-start',
            justifySelf:'self-start',
            margin:'1rem',
            position:'absolute'
        }} onClick={()=>{
            let task = props.data.data;
            task.prompt().then(res=>{
                if(res.outcome==="accepted") {
                    //user has installed -> unrender
                    dispatch({
                        type:'install:hard-disk',
                        payload: {
                            status:false,
                            data: {
                            }
                        }
                    });
                }
            });
        }}>
          <img style={{
              filter: 'drop-shadow(1px 2px 5px rgba(0,0,0,0.6))',
              width: '5rem'
          }} alt="Install" src={installlogo}/>
          {lang.translate('common.installharddisk')}
        </div>
    );
}

function DecoratedDesktop(props) {
    const install = useSelector(state => state.install);
    return (
        <div style={{
            position:'fixed',
            top:0,
            left:0,
            height: 'calc(100% - 4rem)',
            width:'100%',
            display:'grid',
            gridTemplateRows:'repeat(auto-fit, minmax(5vw,50%))',
            gridTemplateColumns:'repeat(auto-fit, minmax(5vw, 50%))',
            alignItems:'end',
            justifyContent:'end',
            gridGap:'0.2rem',
            alignContent:'end'
        }}>
          <ClockWidget/>
          {(install.detected)?<InstallHardDiskWidget data={install}/>:<div></div>}
        </div>
    );
}

export {
    ClockWidget,
    DecoratedDesktop
}
