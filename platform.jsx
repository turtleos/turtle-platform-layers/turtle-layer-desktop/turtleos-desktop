/*
Desktop Platform Layer
 */

import React from 'react';

import { DesktopWM } from './components/desktop-wm/wm';

import { DesktopWindow } from './components/desktop-wm/desktop-window';

const PLATFORM = {
    expects: ( width, height ) => {
        //remove || true when adding more platforms
        if( width > height) return true;
        return false;
    },
    render: () => {
        //Init
        return (
            <DesktopWM/>
        );
    },
    window: (config) => {
        return (
            <DesktopWindow {...config}/>
        );
    }
};

export {
    PLATFORM
}
